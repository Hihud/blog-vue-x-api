import Vue from "vue";
import Router from "vue-router";
import Article from "./views/Article.vue";
import NotFound from "./components/error-page/NotFound.vue";
import ArticleList from "./components/article-components/ArticleList.vue";
import Login from "./components/user-components/Login.vue";
import Register from "./components/user-components/Register.vue";
import ArticleDetail from "./components/article-components/ArticleDetail.vue";
import CreateArticle from "./components/article-components/CreateArticle.vue";
import EditArticle from "./components/article-components/EditArticle.vue";
Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "Home",
      component: Article
    },
    {
      path: "*",
      name: "NotFound",
      component: NotFound
    },
    {
      path: "/article",
      name: "ArticleList",
      component: ArticleList
    },
    {
      path: "/login",
      name: "Login",
      component: Login
    },
    {
      path: "/register",
      name: "Register",
      component: Register
    },
    {
      path: "/article/view/:id",
      name: "ArticleDetail",
      component: ArticleDetail
    },
    {
      path: "/article/create",
      name: "CreateArticle",
      component: CreateArticle
    },
    {
      path: "/article/edit/:id",
      name: "EditArticle",
      component: EditArticle
    }
  ]
});
