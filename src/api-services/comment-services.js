import Axios from "axios";

const RESOURCE_NAME = "http://localhost:59065/api/comment";
export default {
  addComment: function(comment, token) {
    return Axios({
      method: "post",
      url: RESOURCE_NAME + "/addcomment",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token
      },
      params: {
        ArticleId: comment.ArticleId,
        UserId: comment.UserId,
        Content: comment.Content,
        DateCreate: comment.DateCreate
      }
    });
  },
  delete: function(id, token) {
    return Axios.delete(RESOURCE_NAME + "/DeleteComment/" + id, {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token
      }
    });
  }
};
