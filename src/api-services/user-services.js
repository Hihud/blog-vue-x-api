import Axios from "axios";

const RESOURCE_NAME = "http://localhost:59065/api/user";
export default {
  register: function(object) {
    return Axios.post(RESOURCE_NAME + "/register", object);
  },
  login: function(object) {
    return Axios.post(RESOURCE_NAME + "/login", object);
  }
};
