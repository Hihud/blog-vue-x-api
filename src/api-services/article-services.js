import Axios from "axios";

const RESOURCE_NAME = "http://localhost:59065/api/article";

export default {
  getAll: function(page, count) {
    return Axios({
      method: "get",
      url: RESOURCE_NAME + "/getarticle",
      params: {
        page: page,
        count: count
      }
    });
  },
  get: function(id) {
    return Axios.get(RESOURCE_NAME + "/getarticle/" + id);
  },
  createArticle: function(formdata, token) {
    return Axios({
      method: "post",
      url: RESOURCE_NAME + "/create",
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: "Bearer " + token
      },
      data: formdata
    });
  },
  edit: function(formdata, token) {
    return Axios({
      method: "put",
      url: RESOURCE_NAME + "/edit/" + formdata.id,
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: "Bearer " + token
      },
      data: formdata
    });
  },
  delete: function(id, token) {
    return Axios({
      method: "delete",
      url: RESOURCE_NAME + "/delete/" + id,
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token
      },
      params: {
        id: id
      }
    });
  }
};
