import Vue from "vue";
import Vuex from "vuex";
import VuexPersist from "vuex-persist";

Vue.use(Vuex);

const vuexPersist = new VuexPersist({
  key: "app",
  storage: localStorage
});

export default new Vuex.Store({
  state: {
    user: {},
    token: ""
  },
  mutations: {
    setToken: function(state, data) {
      state.token = data.token;
    },
    setUser: function(state, data) {
      state.user = data.user;
    },
    reset: function(state) {
      (state.token = ""), (state.user = {});
    }
  },
  actions: {},
  getters: {
    user: state => state.user,
    token: state => state.token
  },
  plugins: [vuexPersist.plugin]
});
